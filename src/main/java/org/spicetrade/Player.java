/*
 * Spice Trade Copyright (C) 2005 spicetrade.org
 *
 * Author: Juha Holopainen, juhah@spicetrade.org
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package org.spicetrade;

import org.spicetrade.tools.Item;
import org.spicetrade.tools.LogItem;
import org.spicetrade.tools.MapEntry;

import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;

public class Player {

    public static final String[] cities = {"baghdad", "anjoudan", "najaf", "latakia", "konya", "baku", "constantinopol", "venice", "madrid", "lisboa",
            "budapest", "vienna", "moscow", "hamburg", "amsterdam", "london", "paris"};
    public static final String[] niceCities = {"Baghdad", "Anjoudan", "Najaf", "al-Ladiqiyah", "Konya", "Baku", "Constantinople", "Venice", "Madrid",
            "Lisbon", "Budapest", "Vienna", "Moscow", "Hamburg", "Amsterdam", "London", "Paris"};
    public Hashtable<String, String> attr;
    public Vector<Item> soldItems;
    public Vector<Item> items;
    public Vector<LogItem> log;
    public boolean fullScreen;
    public int x;
    public int y;
    public int xHomeCountry;
    public int yHomeCountry;
    public int year;
    public int month;
    public int day;
    public int age;
    public String ageType;
    public double health;
    public int money;
    public int happiness;
    public int moral;
    public int force;
    public int baseForce;
    public int culture;
    public int economy;
    public int wives;
    public int children;
    public String place;
    public String city;
    public String nicecity;
    public String lastPlace;
    public String logo;
    public String lastWords;
    public boolean inGame;
    public boolean inBattle;
    public boolean inGameOver;
    public int transport;
    public Journal journal;
    public boolean addictedHashish;
    public boolean addictedOpium;
    public boolean sickPlague;
    public boolean sickGeneral;
    public boolean sickPoisoned;
    public int deathType;
    public int difficulty;
    public String statusFace = "/pics/navigation/abus/abu_young01.png";

    public Player() {
        this(1);
    }

    public Player(int difficulty) {
        this.difficulty = difficulty;
        attr = new Hashtable<>();
        soldItems = new Vector<>();
        items = new Vector<>();
        journal = new Journal();
        log = new Vector<>();
        x = 0;
        y = 0;
        xHomeCountry = 0;
        yHomeCountry = 0;
        moral = 0;
        inGameOver = false;
        addictedHashish = false;
        addictedOpium = false;
        sickPlague = false;
        inBattle = false;
        lastWords = "";
        ageType = "young";
        attr.put("Name", "Abu Mansur 'abd ar-Rahman al-Qazzaz");
        attr.put("dateAndPlace", "C. 500 AH, Baghdad");
        attr.put("wifes", "");
        attr.put("children", "");

        Random r = new Random();
        int c;
        c = r.nextInt(100);
        while (c < 60)
            c = r.nextInt(100);
        attr.put("culturebaghdad", String.valueOf(c));

        c = r.nextInt(100);
        while (c < 40)
            c = r.nextInt(100);
        attr.put("cultureanjoudan", String.valueOf(c));

        c = r.nextInt(100);
        while (c < 40)
            c = r.nextInt(100);
        attr.put("culturenajaf", String.valueOf(c));

        c = r.nextInt(100);
        while (c < 40)
            c = r.nextInt(100);
        attr.put("culturelatakia", String.valueOf(c));

        c = r.nextInt(100);
        while (c < 20)
            c = r.nextInt(100);
        attr.put("cultureconstantinopol", String.valueOf(c));

        c = r.nextInt(100);
        while (c < 30)
            c = r.nextInt(100);
        attr.put("culturekonya", String.valueOf(c));

        c = r.nextInt(100);
        while (c < 30)
            c = r.nextInt(100);
        attr.put("culturebaku", String.valueOf(c));

        c = r.nextInt(100);
        while (c < 30)
            c = r.nextInt(100);
        attr.put("culturevienna", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 30)
            c = r.nextInt(100);
        attr.put("culturebudapest", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 60)
            c = r.nextInt(100);
        attr.put("culturevenice", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 50)
            c = r.nextInt(100);
        attr.put("culturemadrid", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 40)
            c = r.nextInt(100);
        attr.put("culturelisboa", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 60)
            c = r.nextInt(100);
        attr.put("cultureparis", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 40)
            c = r.nextInt(100);
        attr.put("cultureamsterdam", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 50)
            c = r.nextInt(100);
        attr.put("culturehamburg", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 60)
            c = r.nextInt(100);
        attr.put("culturemoscow", String.valueOf(-c));

        c = r.nextInt(100);
        while (c < 60)
            c = r.nextInt(100);
        attr.put("culturelondon", String.valueOf(-c));

        attr.put("Moneyunit", "Dirham");
        attr.put("Timeunit", "AH");
        attr.put("Special", "Navigation");
        year = 500;
        month = 4;
        day = 7;
        age = 17;
        if (difficulty == 1)
            health = 50;
        else
            health = 80;
        happiness = 30;
        if (difficulty == 1)
            money = 25;
        else
            money = 100;
        if (difficulty == 1)
            baseForce = 2;
        else
            baseForce = 5;
        force = baseForce;
        economy = 0;
        culture = 0;
        wives = 0;
        children = 0;
        place = "";
        lastPlace = "";
        logo = "logoIntro";
        to(141, 421);
        toHomeCountry(249, 135, "baghdad", "Baghdad");
        buyItem("12050", 0); // home
        buyItem("14000", 0); // fields
        buyItem("10000", 0); // basic food
        buyItem("10300", 0); // ability to walk

        if (Mainframe.DEBUG > 0) {
            buyItem("10050", 0);

            //buyItem("10600", 0);
            //buyItem("10610", 0);
            //buyItem("10620", 0);
            //buyItem("10630", 0);
            //buyItem("10640", 0);

            buyItem("10710", 0);
            buyItem("10720", 0);
            buyItem("10750", 0);
            buyItem("10751", 0);
            buyItem("10760", 0);
            buyItem("10770", 0);

            // poem books
            buyItem("10800", 0);
            buyItem("10801", 0);
            buyItem("10802", 0);
            buyItem("10803", 0);
            buyItem("10804", 0);
            buyItem("10805", 0);
            buyItem("10806", 0);
            buyItem("10807", 0);
            buyItem("10808", 0);
            buyItem("10809", 0);
            buyItem("10832", 0);
            buyItem("10833", 0);
            buyItem("10834", 0);
            buyItem("10835", 0);
            buyItem("10836", 0);
            buyItem("10837", 0);
            buyItem("10838", 0);
            buyItem("10839", 0);
            buyItem("10840", 0);
            buyItem("10841", 0);

            buyItem("10810", 0);
            buyItem("10811", 0);
            buyItem("10812", 0);
            buyItem("10820", 0);
            buyItem("10830", 0);
            buyItem("10831", 0);

            buyItem("10650", 0);

            buyItem("10320", 0);
            buyItem("10330", 0);
            buyItem("10340", 0);

            buyItem("10350", 0);
            buyItem("10325", 0);
            buyItem("10335", 0);
            buyItem("10345", 0);
            buyItem("10355", 0);
            //buyItem("10360", 0);
            //buyItem("10365", 0);

            buyItem("10720", 0);
            buyItem("10730", 0);
            buyItem("10731", 0);
            buyItem("10732", 0);
            buyItem("10733", 0);

            buyItem("10740", 0);
            buyItem("10741", 0);
            buyItem("10742", 0);
            buyItem("10743", 0);
            buyItem("10744", 0);
            buyItem("10745", 0);

            buyItem("11000", 0);
            buyItem("11001", 0);
            buyItem("11002", 0);
            buyItem("11003", 0);
            buyItem("11004", 0);
            buyItem("11005", 0);

            buyItem("11010", 0);
            buyItem("11011", 0);
            buyItem("11012", 0);
            buyItem("11013", 0);
            buyItem("11014", 0);
            buyItem("11015", 0);

            buyItem("11020", 0);

            buyItem("11030", 0);
            buyItem("11031", 0);
            buyItem("11040", 0);

            buyItem("11050", 0);
            buyItem("11051", 0);
            buyItem("11052", 0);
            buyItem("11053", 0);
            buyItem("11054", 0);
            buyItem("11055", 0);
            buyItem("11056", 0);

            buyItem("11500", 0);
            buyItem("11510", 0);
            buyItem("11520", 0);
            buyItem("11530", 0);

            buyItem("11600", 0);
            buyItem("11601", 0);
            buyItem("11602", 0);
            buyItem("11603", 0);
            buyItem("11610", 0);
            buyItem("11611", 0);
            buyItem("11620", 0);
            buyItem("11630", 0);
            buyItem("11640", 0);
            buyItem("11650", 0);
            buyItem("11651", 0);
            buyItem("11652", 0);
            buyItem("11660", 0);
            buyItem("11661", 0);
            buyItem("11670", 0);
            buyItem("11671", 0);
            buyItem("11672", 0);
            buyItem("11673", 0);
            buyItem("11680", 0);
            buyItem("11681", 0);

            buyItem("11700", 0);

            buyItem("12000", 0);
            buyItem("12001", 0);
            buyItem("12002", 0);
            buyItem("12003", 0);
            buyItem("12004", 0);

            buyItem("12010", 0);
            buyItem("12020", 0);
            buyItem("12030", 0);
            buyItem("12040", 0);

            buyItem("12060", 0);
            buyItem("12061", 0);
            buyItem("12062", 0);
            buyItem("12063", 0);
            buyItem("12064", 0);
            buyItem("12065", 0);
            buyItem("12066", 0);
            buyItem("12067", 0);
            buyItem("12068", 0);
            buyItem("12069", 0);
            buyItem("12070", 0);

            buyItem("12080", 0);
            buyItem("12081", 0);
            buyItem("12082", 0);
            buyItem("12083", 0);
            buyItem("12084", 0);
            buyItem("12085", 0);
            buyItem("12086", 0);
            buyItem("12087", 0);
            buyItem("12088", 0);
            buyItem("12089", 0);
            buyItem("12090", 0);

            buyItem("14010", 0);
            buyItem("14020", 0);
            buyItem("14030", 0);
            //buyItem("14040", 0);

            buyItem("13000", 0);
            buyItem("13010", 0);

            //buyItem("15000", 0);

            buyItem("16000", 0);
            buyItem("16010", 0);
            buyItem("16020", 0);
            buyItem("16030", 0);
        }

        Mainframe.me.showMapGlobe = false;
        inGame = false;
        transport = MapEntry.TRANSPORT1;
    }

    public String get(String s) {
        try {
            return attr.get(s);
        } catch (Exception exception) {
            return null;
        }
    }

    public boolean has(String n) {
        return attr.containsKey(n);
    }

    public void add(String n) {
        attr.put(n, "");
    }

    public void add(String n, String v) {
        attr.put(n, v);
    }

    public void remove(String n) {
        attr.remove(n);
    }

    public boolean contains(String n, String v) {
        String c = "";
        if (has(n))
            c = get(n);
        return (c.contains(v));
    }

    public void addWife(String name) {
        if (contains("wifes", name))
            return;
        String all = attr.get("wifes");
        if (all == null || all.isEmpty())
            all = name;
        else
            all += ", " + name;
        attr.put("wifes", all);
        wives++;
    }

    public boolean hasWife(String name) {
        return contains("wifes", name);
    }

    public void removeWife(String name) {
        if (!contains("wifes", name))
            return;
        String all = attr.get("wifes");
        if (all.indexOf(name) == 0)
            if (!all.contains(","))
                all = "";
            else
                all = all.substring(all.indexOf(","));
        else
            all = all.substring(all.indexOf(", " + name) + name.length() + 2);
        attr.put("wifes", all);
        wives--;
    }

    public void addChild(String name) {
        if (contains("children", name))
            return;
        String all = attr.get("children");
        if (all == null || all.isEmpty())
            all = name;
        else
            all += ", " + name;
        attr.put("children", all);
        children++;
    }

    public boolean hasChild(String name) {
        return contains("children", name);
    }

    public void removeChild(String name) {
        if (!contains("children", name))
            return;
        String all = attr.get("children");
        if (all.indexOf(name) == 0)
            if (!all.contains(","))
                all = "";
            else
                all = all.substring(all.indexOf(","));
        else
            all = all.substring(all.indexOf(", " + name) + name.length() + 2);
        attr.put("children", all);
        children--;
    }

    public void addLog(String id, String action, int when) {
        if (inGame)
            log.add(new LogItem(id, action, when));
        if (log.size() > 100)
            log.remove(0);
    }

    public void addMoney(int amount) {
        money += amount;
    }

    public void removeMoney(int amount) {
        money -= amount;
    }

    public Vector<Item> getInventory() {
        Vector<Item> inventory = new Vector<>();
        Item item;

        for (int i = 0, j = items.size(); i < j; i++) {
            item = items.elementAt(i);
            if (item.inventory)
                inventory.add(item);
        }

        return inventory;
    }

    public void buyItem(String id) {
        try {
            Item item = Mainframe.me.market.getItem(id);
            buyItem(id, item.price);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buyItem(String id, int price, boolean only) {
        try {
            if (only && !hasItem(id))
                buyItem(id, price);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buyItem(String id, int price, String who) {
        try {
            for (int i = 0, j = soldItems.size(); i < j; i++) {
                Item item = soldItems.elementAt(i);
                if (item.who.equals(who) && item.id.equals(id)) {
                    buyItem(id, price);
                    soldItems.remove(i);
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buyItem(String id, int price) {
        try {
            Mainframe mf = Mainframe.me;
            Item item = mf.market.getItem(id);
            if (Mainframe.DEBUG == 1)
                System.out.println("Buying item: " + item.name + " for price " + price);
            money -= price;
            if ("16000".equals(id) || "16010".equals(id) || "16020".equals(id) || "16030".equals(id))
                item.name += ", " + get("worker");
            mf.sounds.playSound("/music/fx_hit.ogg");
            items.add(item);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean hasSold(String id) {
        return hasSold(id, null);
    }

    public boolean hasSold(String id, String who) {
        boolean ret = false;

        for (Object soldItem : soldItems) {
            Item item = (Item) soldItem;
            if (item.id.equals(id)) {
                if (who == null || who.equals(item.who)) {
                    ret = true;
                    break;
                }
            }
        }

        return ret;
    }

    public void sellItem(String id) {
        try {
            Item item = Mainframe.me.market.getItem(id);
            sellItem(id, item.price);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sellItem(String id, int price) {
        try {
            Mainframe mf = Mainframe.me;
            if (journal.has("field2"))
                journal.done("field2", "§- I sold some spices in " + nicecity);
            money += price;
            mf.sounds.playSound("/music/fx_hit.ogg");
            removeItem(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Vector<Item> getSoldItems(String who) {
        Vector<Item> res = new Vector<>();

        try {
            for (Object soldItem : soldItems) {
                Item item = (Item) soldItem;
                if (who.equals(item.who))
                    res.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return res;
    }

    public void sellItem(String id, int price, String who) {
        Mainframe mf = Mainframe.me;
        if (hasItem(id)) {
            Item item = mf.market.getItem(id);
            removeItem(id);
            item.who = who;
            money += (int) (mf.traders.getSellFactor(who) * price);
            soldItems.add(item);
        }
    }

    public boolean hasItem(String id) {
        return hasItem(id, 1);
    }

    public boolean hasItem(String id, int amount) {
        return hasItem(id, amount, true);
    }

    public boolean hasItem(String id, int amount, boolean givenItems) {
        Item item;
        int count = 0;
        boolean res = false;
        for (Object o : items) {
            item = (Item) o;
            if (item.id.equals(id))
                count++;
        }

        for (Object soldItem : soldItems) {
            item = (Item) soldItem;
            if (item.id.equals(id))
                count++;
        }

        if (attr.containsKey(id) && givenItems)
            count++;

        if (count >= amount)
            res = true;

        return res;
    }

    public boolean hasAnyItems(String[] ids) {
        Item item;
        boolean res = false;
        for (Object o : items) {
            item = (Item) o;
            for (String id : ids)
                if (item.id.equals(id)) {
                    res = true;
                    break;
                }
        }

        return res;
    }

    public boolean hasAllItems(String[] ids) {
        Item item;
        boolean res = false;
        for (Object o : items) {
            item = (Item) o;
            for (String id : ids) res = item.id.equals(id);
        }

        return res;
    }

    public boolean canBuy(String id) {
        try {
            return (money >= Mainframe.me.market.getPrice(id));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean hasMoney(int compare) {
        return money >= compare;
    }

    public boolean hasMoreMoney(int compare) {
        return money > compare;
    }

    public void removeItem(String id) {
        Item item;

        for (int i = 0, j = items.size(); i < j; i++) {
            item = items.get(i);
            if (!item.id.equals(id))
                continue;
            if (Mainframe.DEBUG == 1)
                System.out.println("Removing " + id + " from position " + i);
            items.remove(i);
            break;
        }
    }

    public void giveItem(String id) {
        if (Mainframe.DEBUG == 1)
            System.out.println("Giving item: " + id);
        removeItem(id);
        attr.put(id, "");
    }

    public boolean inPlace(String where) {
        return inPlace(where, false);
    }

    public boolean inPlace(String where, boolean exact) {
        boolean ret;
        if (exact)
            ret = (where.equals(place));
        else
            ret = (place.contains(where));

        if (Mainframe.DEBUG == 1)
            System.out.println("(" + place + ") inplace: " + where + ", exact: " + exact + " = " + ret);
        return ret;
    }

    public void to(int _x, int _y) {
        x = _x;
        y = _y;
    }

    public void toHomeCountry(int _x, int _y, String _city, String _nicecity) {
        xHomeCountry = _x;
        yHomeCountry = _y;
        city = _city;
        nicecity = _nicecity;
    }

    public void nextDay() {
        nextDay(false);
    }

    public void nextDay(int amount) {
        nextDay(amount, false);
    }

    public void nextDay(int amount, boolean travelling) {
        for (int i = 0; i < amount; i++)
            nextDay(travelling);
    }

    public void nextDay(boolean travelling) {
        Mainframe mf = Mainframe.me;

        if (day == 30)
            nextMonth(travelling);
        day++;

        if (health > 300) health = 300; // have to have a max value

        if (travelling)
            switch (transport) {
                case MapEntry.TRANSPORT1: // feet
                    if (difficulty == 1)
                        health -= 0.5;
                    else
                        health -= 0.3;
                    break;
                case MapEntry.TRANSPORT2: // horse
                    if (difficulty == 1)
                        health -= 0.4;
                    else
                        health -= 0.2;
                    break;
                case MapEntry.TRANSPORT3: // caravan
                    if (difficulty == 1)
                        health -= 0.4;
                    else
                        health -= 0.2;
                    break;
                case MapEntry.TRANSPORT4: // suleiman
                    if (difficulty == 1)
                        health -= 0.2;
                    else
                        health -= 0.1;
                    break;
                case MapEntry.TRANSPORT5: // boat
                    if (difficulty == 1)
                        health -= 0.4;
                    else
                        health -= 0.2;
                    break;
                case MapEntry.TRANSPORT6: // borak
                    if (difficulty == 1)
                        health -= 0.2;
                    else
                        health -= 0.1;
                    break;
                case MapEntry.TRANSPORT7: // dog
                    if (difficulty == 1)
                        health -= 0.2;
                    else
                        health -= 0.1;
                    break;
            }

        if (sickGeneral) {
            health -= 0.01;

        } else if (sickPlague) {
            health -= 0.1;
        } else if (sickPoisoned) {
            health -= 1;
        }

        if (sickPoisoned) {
            int value = Integer.parseInt(attr.get("poisoned"));
            value++;
            attr.put("poisoned", String.valueOf(value));
            if (value >= 100)
                mf.doActionOnEntering = "mf.gotoDeath(\"Abu died of poisoning.\", 3);";
        }

        if (health < 1) {
            // died, game over
            mf.doActionOnEntering = "mf.gotoDialog(\"9110\");";
        }
    }

    public void nextMonth() {
        nextMonth(false);
    }

    public void nextMonth(boolean travelling) {
        day = 1;
        month++;

        if (month > 12) {
            month = 1;
            nextYear();
        }

        int months = age * 12 + month;

        Mainframe mf = Mainframe.me;
        mf.nextInt = mf.random.nextInt(100);
        ageType = "young";

        int randomEffect = 0;
        force = baseForce;
        for (Object o : items) {
            Item item = (Item) o;
            if (item.id.length() == 5) {
                health += item.health;
                money += item.monthlyCost;
                culture += item.culture;
                economy += item.economy;
                force += item.force;
                happiness += item.happiness;
                randomEffect += item.random;
            }
        }

        if ((hasItem("11000") && hasItem("11001") && hasItem("11002") && hasItem("11003") && hasItem("11004") && hasItem("11005"))
                || (hasItem("11010") && hasItem("11011") && hasItem("11012") && hasItem("11013") && hasItem("11014") && hasItem("11015"))) {
            logo = "logoAmulet";
            happiness++;
            health += 5;
            baseForce++;
            randomEffect += 5;
        } else {
            // Yima
            if (journal.has("yima1") && !journal.contains("yima1", "cursed by Yima")) {
                if (Mainframe.DEBUG == 1)
                    System.out.println("yima random, nextInt: " + mf.nextInt + ", randomEffect: " + randomEffect);

                int value = Integer.parseInt(journal.get("yima1").substring(1));
                value--;
                journal.put("yima1", "!" + value);

                if (value < 1) {
                    if (mf.rB(0, 25 - randomEffect)) {
                        journal.put("yima1", "§- I was cursed by Yima");
                        journal.open("random2");
                        logo = "logoEvil";
                    } else if (mf.rB(25, 50 - randomEffect)) {
                        journal.put("yima1", "§- I was cursed by Yima");
                        journal.open("random3");
                        logo = "logoEvil";
                    } else if (mf.rB(50, 65 - randomEffect)) {
                        journal.put("yima1", "§- I was cursed by Yima");
                        journal.open("random1");
                        logo = "logoEvil";
                    } else if (mf.rB(65, 70 - randomEffect)) {
                        journal.put("yima1", "§- I was cursed by Yima");
                        addAddiction("plague", 10);
                        logo = "logoEvil";
                    }
                }
            }
        }

        // Iblis
        if (journal.has("iblis1") && journal.get("iblis1").charAt(0) == '!') {
            if (Mainframe.DEBUG == 1)
                System.out.println("iblis random, nextInt: " + mf.nextInt + ", randomEffect: " + randomEffect);
            int value = Integer.parseInt(journal.get("iblis1").substring(1));
            value--;
            journal.put("iblis1", "!" + value);
            if (value <= 0 && mf.rB(0, 75 - randomEffect)) {
                mf.doActionOnEntering = "mf.gotoPlace(\"supernaturalbad2\");mf.gotoDialog(\"1571\");";
                logo = "logoEvil";
            }
        }

        // permits
        String permit = "permit1";
        if (journal.has(permit) && !journal.get(permit).isEmpty() && journal.get(permit).charAt(0) == '!') {
            // travel
            int value = Integer.parseInt(journal.get(permit).substring(1));
            value--;
            journal.put(permit, "!" + value);
            if (value <= 0)
                journal.put(permit, "§- The permit can be retrieved from the sultan's official");
        }
        permit = "permit2";
        if (journal.has(permit) && !journal.get(permit).isEmpty() && journal.get(permit).charAt(0) == '!') {
            // museum
            int value = Integer.parseInt(journal.get(permit).substring(1));
            value--;
            journal.put(permit, "!" + value);
            if (value <= 0)
                journal.put(permit, "§- The permit can be retrieved from the sultan's official");
        }
        permit = "permit3";
        if (journal.has(permit) && !journal.get(permit).isEmpty() && journal.get(permit).charAt(0) == '!') {
            // mosque
            int value = Integer.parseInt(journal.get(permit).substring(1));
            value--;
            journal.put(permit, "!" + value);
            if (value <= 0)
                journal.put(permit, "§- The permit can be retrieved from the sultan's official");
        }
        permit = "permit4";
        if (journal.has(permit) && !journal.get(permit).isEmpty() && journal.get(permit).charAt(0) == '!') {
            // export
            int value = Integer.parseInt(journal.get(permit).substring(1));
            value--;
            journal.put(permit, "!" + value);
            if (value <= 0)
                journal.put(permit, "§- The permit can be retrieved from the sultan's official");
        }
        permit = "permit10";
        if (journal.has(permit) && !journal.get(permit).isEmpty() && journal.get(permit).charAt(0) == '!') {
            // library
            int value = Integer.parseInt(journal.get(permit).substring(1));
            value--;
            journal.put(permit, "!" + value);
            if (value <= 0)
                journal.put(permit, "§- The permit can be retrieved from the sultan's official");
        }
        permit = "permit11";
        if (journal.has(permit) && !journal.get(permit).isEmpty() && journal.get(permit).charAt(0) == '!') {
            // shrine
            int value = Integer.parseInt(journal.get(permit).substring(1));
            value--;
            journal.put(permit, "!" + value);
            if (value <= 0)
                journal.put(permit, "§- The permit can be retrieved from the sultan's official");
        }
        permit = "permit12";
        if (journal.has(permit) && !journal.get(permit).isEmpty() && journal.get(permit).charAt(0) == '!') {
            // shrine
            int value = Integer.parseInt(journal.get(permit).substring(1));
            value--;
            journal.put(permit, "!" + value);
            if (value <= 0)
                journal.put(permit, "§- The permit can be retrieved from the king's official");
        }

        // Umm
        if (journal.has("abdullah2") && journal.get("abdullah2").charAt(0) == '!') {
            // FIX 13.5.2005 Getting married to Umm needed some changes to make it foolproof(er)
            String parse = journal.get("abdullah2").substring(1, 2);
            String other = journal.get("abdullah2").substring(2);
            journal.put("abdullah2", other + "§- I waited one month to see Umm again");
        }

        // culture
        for (String value : cities) {
            Iterable<Item> museumItems;
            if (has("museum" + value)) {
                if ("baghdad".equals(value)) {
                    museumItems = itemsVector("museum");
                    for (Object museumItem : museumItems) {
                        Item item = (Item) museumItem;
                        for (int k = 0, l = cities.length; k < l; k++) {
                            addCulture(cities[l], item.culture / 2);
                        }
                    }
                } else {
                    museumItems = itemsVector(value + "museum");
                    for (Object museumItem : museumItems) {
                        Item item = (Item) museumItem;
                        for (int k = 0, l = cities.length; k < l; k++) {
                            addCulture(cities[l], item.culture / 4);
                        }
                        addCulture(value, item.culture);
                    }
                }
            }

            if (getCulture(value) < 0) {
                boolean destroy = false;
                // they don't appreciate you
                if (has("museum" + value) && !journal.contains("permit" + value, "destroyed")) {
                    if (mf.doActionOnEntering.isEmpty()) {
                        switch (value) {
                            case "amsterdam":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3340\");";
                                break;
                            case "venice":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3341\");";
                                break;
                            case "budapest":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3342\");";
                                break;
                            case "vienna":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3343\");";
                                break;
                            case "moscow":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3344\");";
                                break;
                            case "madrid":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3345\");";
                                break;
                            case "lisboa":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3346\");";
                                break;
                            case "paris":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3347\");";
                                break;
                            case "london":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3348\");";
                                break;
                            case "hamburg":
                                mf.doActionOnEntering = "mf.gotoDialog(\"3349\");";
                                break;
                        }

                        // destroy museum and collections in there
                        destroy = true;
                        if ("baghdad".equals(value)) {
                            museumItems = itemsVector("museum");
                            for (Object museumItem : museumItems) {
                                Item item = (Item) museumItem;
                                giveItem(item.id);
                            }
                        } else {
                            museumItems = itemsVector(value + "museum");
                            for (Object museumItem : museumItems) {
                                Item item = (Item) museumItem;
                                giveItem(item.id);
                            }
                        }

                        journal.add("permit" + value, "§- They destroyed my museum and my collections");
                        add("removemuseum" + value);
                    }
                }

                if (has("mosque" + value)) {
                    if ((destroy || mf.doActionOnEntering.isEmpty()) && !journal.contains("church" + value, "mosque is gone")) {
                        switch (value) {
                            case "amsterdam":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3330\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3320\");";
                                break;
                            case "venice":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3331\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3321\");";
                                break;
                            case "budapest":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3332\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3322\");";
                                break;
                            case "vienna":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3333\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3323\");";
                                break;
                            case "moscow":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3334\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3324\");";
                                break;
                            case "madrid":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3335\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3325\");";
                                break;
                            case "lisboa":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3336\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3326\");";
                                break;
                            case "paris":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3337\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3327\");";
                                break;
                            case "london":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3338\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3328\");";
                                break;
                            case "hamburg":
                                if (destroy)
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3339\");";
                                else
                                    mf.doActionOnEntering = "mf.gotoDialog(\"3329\");";
                                break;
                        }

                        // destroy mosque
                        journal.add("church" + value, "§- The mosque is gone and a church stands in it's place");
                        add("removemosque" + value);
                    }
                }

            } else if (getCulture(value) > 49 && !has("museum" + value)) {
                // they really like you and want you to build a museum
                if (!has("museum" + value) && !journal.has("permit" + value) && !journal.isDone("permit" + value)
                        && mf.doActionOnEntering.isEmpty())
                    if ("amsterdam".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3300\");";
                    else if ("venice".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3301\");";
                    else if ("budapest".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3302\");";
                    else if ("vienna".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3303\");";
                    else if ("moscow".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3304\");";
                    else if ("madrid".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3305\");";
                    else if ("lisboa".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3306\");";
                    else if ("paris".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3307\");";
                    else if ("london".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3308\");";
                    else if ("hamburg".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3309\");";
            } else if (getCulture(value) > 79) {
                // they worship you and will change their churches to mosques
                if (!has("mosque" + value) && !journal.has("church" + value) && !journal.isDone("church" + value)
                        && mf.doActionOnEntering.isEmpty())
                    if ("amsterdam".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3310\");";
                    else if ("venice".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3311\");";
                    else if ("budapest".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3312\");";
                    else if ("vienna".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3313\");";
                    else if ("moscow".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3314\");";
                    else if ("madrid".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3315\");";
                    else if ("lisboa".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3316\");";
                    else if ("paris".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3317\");";
                    else if ("london".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3318\");";
                    else if ("hamburg".equals(value))
                        mf.doActionOnEntering = "mf.gotoDialog(\"3319\");";
            }
        }

        if (getCulture("baghdad") < 0 && !has("churchbaghdad")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("churchbaghdad");
                mf.doActionOnEntering = "mf.gotoDialog(\"5100\");";
            }
        } else if (getCulture("najaf") < 0 && !has("churchnajaf")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("churchnajaf");
                mf.doActionOnEntering = "mf.gotoDialog(\"5101\");";
            }
        } else if (getCulture("anjoudan") < 0 && !has("churchanjoudan")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("churchanjoudan");
                mf.doActionOnEntering = "mf.gotoDialog(\"5102\");";
            }
        } else if (getCulture("latakia") < 0 && !has("churchlatakia")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("churchlatakia");
                mf.doActionOnEntering = "mf.gotoDialog(\"5103\");";
            }
        } else if (getCulture("konya") < 0 && !has("churchkonya")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("churchkonya");
                mf.doActionOnEntering = "mf.gotoDialog(\"5104\");";
            }
        } else if (getCulture("baku") < 0 && !has("churchbaku")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("churchbaku");
                mf.doActionOnEntering = "mf.gotoDialog(\"5105\");";
            }
        } else if (getCulture("constantinopol") < 0 && !has("churchconstantinopol")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("churchconstantinopol");
                mf.doActionOnEntering = "mf.gotoDialog(\"5106\");";
            }
        }

        if (getCulture("baghdad") > 9 && has("churchbaghdad")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("mosquebaghdad");
                remove("churchbaghdad");
                mf.doActionOnEntering = "mf.gotoDialog(\"5110\");";
            }
        } else if (getCulture("najaf") > 9 && has("churchnajaf")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("mosquenajaf");
                remove("churchnajaf");
                mf.doActionOnEntering = "mf.gotoDialog(\"5111\");";
            }
        } else if (getCulture("anjoudan") > 9 && has("churchanjoudan")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("mosqueanjoudan");
                remove("churchanjoudan");
                mf.doActionOnEntering = "mf.gotoDialog(\"5112\");";
            }
        } else if (getCulture("latakia") > 9 && has("churchlatakia")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("mosquelatakia");
                remove("churchlatakia");
                mf.doActionOnEntering = "mf.gotoDialog(\"5113\");";
            }
        } else if (getCulture("konya") > 9 && has("churchkonya")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("mosquekonya");
                remove("churchkonya");
                mf.doActionOnEntering = "mf.gotoDialog(\"5114\");";
            }
        } else if (getCulture("baku") > 9 && has("churchbaku")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("mosquebaku");
                remove("churchbaku");
                mf.doActionOnEntering = "mf.gotoDialog(\"5115\");";
            }
        } else if (getCulture("constantinopol") > 9 && has("churchconstantinopol")) {
            if (mf.doActionOnEntering.isEmpty()) {
                add("mosqueconstantinopol");
                remove("churchconstantinopol");
                mf.doActionOnEntering = "mf.gotoDialog(\"5116\");";
            }
        }

        if (difficulty == 0) {
            if (age > 19 && (money > 10000 || journal.isDone("mahdi1") || journal.isDone("yima1") || journal.isDone("grave3")) && !journal.has("permit2")
                    && !journal.isDone("permit2") && mf.doActionOnEntering.isEmpty()) {
                journal.open("permit2");
                journal.open("");
                add("buildingpermit");
                mf.doActionOnEntering = "mf.gotoDialog(\"3110\");";
            } else if (age > 24 && (money > 10000 || journal.isDone("mahdi1") || journal.isDone("yima1") || journal.isDone("grave3"))
                    && !journal.has("permit10") && !journal.isDone("permit10") && mf.doActionOnEntering.isEmpty() && journal.isDone("permit2")) {
                journal.open("permit10");
                add("buildingpermit");
                mf.doActionOnEntering = "mf.gotoDialog(\"3080\");";
            }
        } else if (difficulty == 1) {
            if (age > 25 && (money > 20000 || journal.isDone("mahdi1") || journal.isDone("yima1") || journal.isDone("grave5")) && !journal.has("permit2")
                    && !journal.isDone("permit2") && mf.doActionOnEntering.isEmpty()) {
                journal.open("permit2");
                add("buildingpermit");
                mf.doActionOnEntering = "mf.gotoDialog(\"3110\");";
            } else if (age > 29 && (money > 20000 || journal.isDone("mahdi1") || journal.isDone("yima1") || journal.isDone("grave5"))
                    && !journal.has("permit10") && !journal.isDone("permit10") && mf.doActionOnEntering.isEmpty() && journal.isDone("permit2")) {
                journal.open("permit10");
                add("buildingpermit");
                mf.doActionOnEntering = "mf.gotoDialog(\"3080\");";
            }
        }

        // addictions
        if (addictedHashish) {
            if (hasItem("10220")) {
                removeItem("10220");
                addLog("You smoked hashish", "res=false;", day + month * 30 + year * 360);
            } else {
                health -= 5;
                addLog("You had no hashish, even though you are addicted to it. You feel weaker.", "res=false;", day + month * 30 + year * 360);
            }
        } else if (addictedOpium) {
            if (hasItem("10230")) {
                removeItem("10230");
                addLog("You smoked opium", "res=false;", day + month * 30 + year * 360);
            } else {
                health -= 20;
                addLog("You had no opium, even though you are addicted to it. You feel weaker.", "res=false;", day + month * 30 + year * 360);
            }
        }

        String[] armies = {"venice", "lisboa", "paris", "hamburg", "moscow", "budapest", "vienna", "madrid", "london", "amsterdam"};
        Random r = new Random();
        int army = r.nextInt(9);
        if (!journal.isDone("random1") && hasItem("16020") && age > 18) {
            add("foreignarmy", armies[0]);
            journal.open("random1", "§- The army of " + mf.armies.getName(armies[0]) + " is attacking your country!");
        } else if (!journal.isDone("random4") && journal.isDone("random1") && hasItem("16020") && age > 20) {
            add("foreignarmy", armies[1]);
            journal.open("random4", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        } else if (!journal.isDone("random5") && journal.isDone("random4") && hasItem("16020") && age > 23) {
            add("foreignarmy", armies[2]);
            journal.open("random5", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        } else if (!journal.isDone("random6") && journal.isDone("random5") && hasItem("16020") && age > 24) {
            add("foreignarmy", armies[3]);
            journal.open("random6", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        } else if (!journal.isDone("random7") && journal.isDone("random6") && hasItem("16020") && age > 27) {
            add("foreignarmy", armies[4]);
            journal.open("random7", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        } else if (!journal.isDone("random8") && journal.isDone("random7") && hasItem("16020") && age > 29) {
            add("foreignarmy", armies[5]);
            journal.open("random8", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        } else if (!journal.isDone("random9") && journal.isDone("random8") && hasItem("16020") && age > 34) {
            add("foreignarmy", armies[6]);
            journal.open("random9", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        } else if (!journal.isDone("random10") && journal.isDone("random9") && hasItem("16020") && age > 35) {
            add("foreignarmy", armies[7]);
            journal.open("random10", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        } else if (!journal.isDone("random11") && journal.isDone("random10") && hasItem("16020") && age > 39) {
            add("foreignarmy", armies[8]);
            journal.open("random11", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        } else if (!journal.isDone("random12") && journal.isDone("random11") && hasItem("16020") && age > 44) {
            add("foreignarmy", armies[9]);
            journal.open("random12", "§- The army of " + mf.armies.getName(armies[1]) + " is attacking your country!");
        }

        int allCulture = 0;
        for (String s : cities) allCulture += getCulture(s);
        culture = allCulture / cities.length;

        String[] shopCities = {"amsterdam", "budapest", "constantinopol", "hamburg", "lisboa", "london", "madrid", "moscow", "paris", "venice", "vienna"};
        String[] shops = {"12080", "12081", "12082", "12083", "12084", "12085", "12086", "12087", "12088", "12089", "12090"};
        for (int i = 0, j = shops.length; i < j; i++)
            if (hasItem(shops[i]))
                putShopMoney(shopCities[i], 150);

        chooseFace(true);
    }

    public void nextYear() {
        Mainframe mf = Mainframe.me;
        Random r = new Random();
        age++;
        year++;
        baseForce += 3;
        if (difficulty == 0) // easy level
            baseForce += 2;
        // reset map piece counters
        for (String s : cities) attr.remove(s + "mappiece");

        // culture
        String[] ccities = {"venice", "lisboa", "paris", "hamburg", "moscow", "budapest", "vienna", "madrid", "london", "amsterdam"};
        int[] cultures = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
        int whole = 0;
        for (int i = 0, j = ccities.length; i < j; i++) {
            whole += cultures[i];
            if (r.nextInt(10) > 7)
                removeCulture(ccities[i], r.nextInt((int) cultures[i]));
            else
                addCulture(ccities[i], r.nextInt((int) cultures[i]));
        }

        if (r.nextInt(10) > 7) {
            addCulture("baghdad", whole / 10);
            addCulture("najaf", whole / 8);
            addCulture("anjoudan", whole / 8);
            addCulture("latakia", whole / 6);
            addCulture("konya", whole / 6);
            addCulture("baku", whole / 6);
            addCulture("constantinopol", whole / 3);
        } else {
            removeCulture("baghdad", whole / 10);
            removeCulture("najaf", whole / 8);
            removeCulture("anjoudan", whole / 8);
            removeCulture("latakia", whole / 6);
            removeCulture("konya", whole / 6);
            removeCulture("baku", whole / 6);
            removeCulture("constantinopol", whole / 3);
        }

        // died of age
        if ((age > 50 && health < 60 && r.nextInt(100) < age) || (age > 70 && r.nextBoolean())) {
            mf.doActionOnEntering = "mf.gotoDialog(\"9120\");";
        }
    }

    public void chooseFace() {
        chooseFace(false, "");
    }

    public void chooseFace(String number) {
        chooseFace(false, number);
    }

    public void chooseFace(boolean nextMonth) {
        chooseFace(nextMonth, "");
    }

    public void chooseFace(boolean nextMonth, String number) {
        Mainframe mf = Mainframe.me;

        String beginning = "/pics/navigation/abus/abu_";

        if (age > 49)
            ageType = "old";
        else if (age > 29)
            ageType = "middleaged";
        else
            ageType = "young";

        if (number.isEmpty()) {
            if (health < 10)
                statusFace = beginning + ageType + "08.png";
            else if (health < 20)
                statusFace = beginning + ageType + "12.png";
            else if (health < 35) {
                if (moral < -75) {
                    statusFace = beginning + ageType + "07.png";
                } else if (moral > 75) {
                    statusFace = beginning + ageType + "17.png";
                } else
                    statusFace = beginning + ageType + "04.png";
            } else if (health < 60) {
                if (moral < -75) {
                    statusFace = beginning + ageType + "07.png";
                } else if (moral > 75) {
                    statusFace = beginning + ageType + "17.png";
                } else if (money < 200 && mf.rB(0, 2))
                    statusFace = beginning + ageType + "15.png";
                else
                    statusFace = beginning + ageType + "01.png";
            } else if (health < 75) {
                if (moral < -75) {
                    statusFace = beginning + ageType + "07.png";
                } else if (moral > 75) {
                    statusFace = beginning + ageType + "17.png";
                } else
                    statusFace = beginning + ageType + "06.png";
            } else if (health < 90) {
                if (moral < -75) {
                    statusFace = beginning + ageType + "07.png";
                } else if (moral > 75) {
                    statusFace = beginning + ageType + "17.png";
                } else
                    statusFace = beginning + ageType + "09.png";
            } else {
                if (moral < -75) {
                    statusFace = beginning + ageType + "07.png";
                } else if (moral > 75) {
                    statusFace = beginning + ageType + "17.png";
                } else
                    statusFace = beginning + ageType + "11.png";
            }
        } else
            statusFace = beginning + ageType + number + ".png";
    }

    public void addGood(int amount) {
        addBad(-amount);
    }

    public void addBad(int amount) {
        Mainframe mf = Mainframe.me;

        if (moral >= (-100 - amount) && moral <= (100 + amount)) {
            moral -= amount;
        }

        chooseFace();
    }

    public void addAddiction(String addiction, int amount) {
        Mainframe mf = Mainframe.me;
        int value = 0;
        if (attr.containsKey(addiction))
            value = Integer.parseInt(attr.get(addiction));
        value += amount;
        attr.put(addiction, String.valueOf(value));

        switch (addiction) {
            case "hashish":
                if (value > 20 && mf.rB(0, 60))
                    addictedHashish = true;
                else if (value < 20)
                    addictedHashish = false;
                break;
            case "opium":
                if (value > 20 && mf.rB(0, 80))
                    addictedOpium = true;
                else if (value < 20)
                    addictedOpium = false;
                break;
            case "sick":
                if (value > 0)
                    sickGeneral = true;
                else if (value <= 0)
                    sickGeneral = false;
                break;
            case "poisoned":
                if (value > 0)
                    sickPoisoned = true;
                else if (value <= 0)
                    sickPoisoned = false;
                break;
            case "plague":
                if (value > 0)
                    sickPlague = true;
                else if (value <= 0)
                    sickPlague = false;
                break;
        }

        logo = "logoIntro";

        if (addictedHashish)
            logo = "logoHashish";
        if (addictedOpium)
            logo = "logoOpium";
        if (sickPlague || sickGeneral || sickPoisoned)
            logo = "logoPlague";

        chooseFace();
    }

    public void removeAddiction(String addiction) {
        switch (addiction) {
            case "hashish":
                attr.remove(addiction);
                addictedHashish = false;
                break;
            case "opium":
                attr.remove(addiction);
                addictedOpium = false;
                break;
            case "sick":
                attr.remove(addiction);
                sickGeneral = false;
                break;
            case "poisoned":
                attr.remove(addiction);
                sickPoisoned = false;
                break;
            case "plague":
                attr.remove(addiction);
                sickPlague = false;
                break;
        }
        logo = "logoIntro";
        chooseFace();
    }

    public void removeAddictions() {
        removeAddiction("hashish");
        removeAddiction("opium");
        removeAddiction("sick");
        removeAddiction("poisoned");
        removeAddiction("plague");
    }

    public void switchItem(String id, String id2) {
        switchItem(id, id2, false);
    }

    public void switchItem(String id, String id2, boolean out) {
        try {
            if (Mainframe.DEBUG == 1)
                System.out.println("switching item: " + id + " to " + id2);
            if (!hasItem(id))
                return;
            Mainframe mf = Mainframe.me;
            removeItem(id);
            Item item = mf.market.getItem(id2);
            if (!out)
                item.where = place;
            items.add(item);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean itemHere(String id) {
        for (Object o : items) {
            Item item = (Item) o;
            if (item.id.equals(id) && item.here())
                return true;
        }
        return false;
    }

    public int itemsHere() {
        return itemsHere(place);
    }

    public int itemsHere(String place) {
        int counter = 0;
        for (Object o : items) {
            Item item = (Item) o;
            if (item.here(place))
                counter++;
        }
        if (Mainframe.DEBUG == 2)
            System.out.println("Items here: " + counter);
        return counter;
    }

    public Vector<Item> itemsVector(String place) {
        Vector<Item> ret = new Vector<>();

        int counter = 0;
        for (Object o : items) {
            Item item = (Item) o;
            if (item.here(place))
                ret.add(item);
        }
        if (Mainframe.DEBUG == 2)
            System.out.println("Items here: " + ret);
        return ret;
    }

    public void addCulture(String where, int amount) {
        removeCulture(where, -amount);
    }

    public void removeCulture(String where, int amount) {
        try {
            String parse = attr.get("culture" + where);
            int value = Integer.parseInt(parse);
            amount = -amount;
            if (Mainframe.DEBUG == 1)
                System.out.println("Changing culture of " + where + " (" + value + "): " + amount);
            value += amount;
            attr.put("culture" + where, String.valueOf(value));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getCulture(String where) {
        try {
            String parse = attr.get("culture" + where);
            int value = Integer.parseInt(parse);
            if (Mainframe.DEBUG == 1)
                System.out.println("Getting culture of " + where + ": " + value);
            return value;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public void getShopMoney(String where) {
        try {
            String[] shops = {"amsterdam", "budapest", "constantinopol", "hamburg", "lisboa", "london", "madrid", "moscow", "paris", "venice", "vienna"};

            if ("all".equals(where)) {
                for (String string : shops) {
                    if (has(where + "shop")) {
                        String parse = attr.get(where + "shop");
                        int value = Integer.parseInt(parse);
                        if (Mainframe.DEBUG == 1)
                            System.out.println("Getting money from shop in " + where + ": " + value);
                        money += value;
                        attr.put(where + "shop", "0");
                    }
                }
            } else if (has(where + "shop")) {
                String parse = attr.get(where + "shop");
                int value = Integer.parseInt(parse);
                if (Mainframe.DEBUG == 1)
                    System.out.println("Getting money from shop in " + where + ": " + value);
                money += value;
                attr.put(where + "shop", "0");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void putShopMoney(String where, int amount) {
        try {
            if (has(where + "shop")) {
                String parse = attr.get(where + "shop");
                int value = Integer.parseInt(parse);
                if (Mainframe.DEBUG == 1)
                    System.out.println("Putting money to shop in " + where + " (+" + amount + "): " + value);
                value += amount;
                attr.put(where + "shop", String.valueOf(value));
            } else
                attr.put(where + "shop", "0");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}